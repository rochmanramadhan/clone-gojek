import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

const ScrollableItem = (props) => {
    return (
        <View style={{marginRight: 16, width: 150}}>
            <View style={{height: 150,width: 150}}>
                <Image source={props.img} style={{width: undefined,height: undefined,resizeMode: 'cover',flex: 1,borderRadius: 6}}/>
            </View>
            <Text style={{fontSize: 16, color: '#1C1C1C', fontWeight: 'bold', marginTop: 12, width: '100%'}}>{props.title}</Text>
        </View>
    )
}

export default ScrollableItem;

const styles = StyleSheet.create({})
