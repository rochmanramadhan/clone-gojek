import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const MainFeature = (props) => {
    return (
        <View style={{alignItems: 'center', width: '25%', marginBottom: 18}}>
            <View style={styles.itemMainMenu}>
            <Image source={props.img} />
            </View>
            <Text style={styles.itemMainMenuText}>{props.title}</Text>
        </View>
    )
}

export default MainFeature;

const styles = StyleSheet.create({
    itemMainMenu: {
        width: 58,
        height: 58,
        borderWidth: 1,
        borderColor: '#EFEFEF',
        borderRadius: 18,
        justifyContent: 'center',
        alignItems: 'center',
      },
      itemMainMenuText: {
        fontSize: 11,
        fontWeight: 'bold',
        alignSelf: 'center',
        marginTop: 6,
      },
})
