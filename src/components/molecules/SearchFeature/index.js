import React from 'react';
import { StyleSheet, TextInput, View, Image } from 'react-native';

const SearchFeature = (props) => {
    return (
        <View
          style={{marginHorizontal: 17, flexDirection: 'row', paddingTop: 15}}>
          <View style={{position: 'relative', flex: 1}}>
            <TextInput placeholder="What do you want to eat?" style={styles.searchBar}/>
            <Image source={require('../../../assets/icon/search.png')} style={styles.searchIcon} />
          </View>
          <View style={{width: 35, alignItems: 'center', justifyContent: 'center'}}>
            <Image source={require('../../../assets/icon/promo.png')} />
          </View>
        </View>
    )
}

export default SearchFeature;

const styles = StyleSheet.create({
    searchBar: {
        borderWidth: 1,
        borderColor: '#E8E8E8',
        borderRadius: 25,
        height: 40,
        fontSize: 13,
        paddingLeft: 45,
        marginRight: 18,
        paddingRight: 20,
        backgroundColor: 'white',
      },
      searchIcon: {
        position: 'absolute',
        top: 6,
        left: 12,
      },
})
