import React from 'react';
import { StyleSheet, Text, View, Image} from 'react-native';

const BottomNavItem = (props) => {
    return (
        <View style={styles.itemIconWrapper}>
          <Image source={props.img}style={styles.itemIcon}/>
            <Text style={{fontSize: 10, color: props.active ? '#43AB4A' : '#545454', marginTop: 4}}>{props.title}</Text>
        </View>
    )
}

export default BottomNavItem;

const styles = StyleSheet.create({
    itemIconWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      itemIcon: {
        height: 26,
        width: 26,
      },
})
