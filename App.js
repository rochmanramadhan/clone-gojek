/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {ScrollView, View} from 'react-native';
import GoBanner from './src/components/molecules/GoBanner';
import GoInfo from './src/components/molecules/GoInfo';
import GoNews from './src/components/molecules/GoNews';
import SearchFeature from './src/components/molecules/SearchFeature';
import BottomNav from './src/containers/organisms/BottomNav';
import HomeGoPay from './src/containers/organisms/HomeGoPay';
import HomeMainFeature from './src/containers/organisms/HomeMainFeature';
import ScrollableProducts from './src/containers/organisms/ScrollableProducts';

const App = () => {
  return (
    <View style={{flex: 1}}>
      <ScrollView style={{backgroundColor: 'white', flex: 1}}>
        {/* search bar */}
        <SearchFeature />
        {/* gopay */}
        <HomeGoPay />
        {/* main features */}
        <HomeMainFeature />
        <View style={{height: 17, backgroundColor: '#F2F2F4', marginTop: 20}} />
        {/* News Section */}
        <GoNews />
        {/* Internal Information Section */}
        <GoInfo />
        {/* GO-FOOD Banner Section */}
        <GoBanner />
        {/* Nearby Go-Food */}
        <ScrollableProducts />
      </ScrollView>
      {/* BottomNavigationView */}
      <BottomNav />
    </View>
  );
};

export default App;
