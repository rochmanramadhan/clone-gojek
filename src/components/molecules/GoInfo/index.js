import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

const GoInfo = () => {
    return (
        <View style={{padding: 16, paddingBottom: 0}}>
          <View style={{height: 15,width: 60,marginLeft: -4,}}>
            <Image source={require('../../../assets/logo/gojek.png')} style={{width: undefined,height: undefined,resizeMode: 'contain',flex: 1,}}/>
          </View>
          <Text style={{fontSize: 17,fontWeight: 'bold',color: '#1C1C1C',marginBottom: 20,marginTop: 15}}>Complete your profile</Text>
          <View style={{flexDirection: 'row', marginBottom: 16}}>
            <View>
              <Image source={require('../../../assets/dummy/facebook-connect.png')} />
            </View>
            <View style={{marginLeft: 16, flex: 1}}>
              <Text style={{fontWeight: 'bold', fontSize: 15, color: '#4A4A4A'}}>Connect with Facebook</Text>
              <Text style={{fontWeight: 'normal',fontSize: 15,color: '#4A4A4A',width: '70%'}}>Login faster without verfication code</Text>
            </View>
          </View>
          <TouchableOpacity style={{backgroundColor: '#61A756',padding: 11,borderRadius: 4,alignSelf: 'flex-end'}}>
            <Text style={{fontSize: 13,fontWeight: 'bold',color: 'white',textAlign: 'center'}}>CONNECT</Text>
          </TouchableOpacity>
          <View style={{borderBottomColor: '#E8E9ED',borderBottomWidth: 1,marginTop: 16,}}/>
        </View>
    )
}

export default GoInfo;

const styles = StyleSheet.create({})
