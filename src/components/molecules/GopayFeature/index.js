import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

const GopayFeature = (props) => {
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <Image source={props.img} />
        <Text style={{fontSize: 13,color: 'white',fontWeight: 'bold',marginTop: 15}}>{props.title}</Text>
      </View>
    )
}

export default GopayFeature;

const styles = StyleSheet.create({})
