import React from 'react';
import { StyleSheet, Text, View, Image} from 'react-native';
import BottomNavItem from '../../../components/molecules/BottomNavItem';

const BottomNav = () => {
    return (
        <View style={styles.itemContainer}>
        <BottomNavItem title="Home" img={require('../../../assets/icon/home-active.png')} active/>
        <BottomNavItem title="Orders" img={require('../../../assets/icon/order.png')}/>
        <BottomNavItem title="Help" img={require('../../../assets/icon/help.png')}/>
        <BottomNavItem title="Inbox" img={require('../../../assets/icon/inbox.png')}/>
        <BottomNavItem title="Account" img={require('../../../assets/icon/account.png')}/>
      </View>
    )
}

export default BottomNav;

const styles = StyleSheet.create({
    itemContainer: {
        height: 57,
        borderColor: '#E8E8E8',
        borderWidth: 1,
        flexDirection: 'row',
        backgroundColor: 'white',
      },
})
